package PhoneBook::Controller::Root;
use Moose;
use namespace::autoclean;
use System::User;
use System::Numbers;
use Data::Dumper;

BEGIN { extends 'Catalyst::Controller' }

#
# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in MyApp.pm
#
__PACKAGE__->config(namespace => '');

=encoding utf-8

=head1 NAME

PhoneBook::Controller::Root - Root Controller for PhoneBook

=head1 DESCRIPTION

[enter your description here]

=head1 METHODS

=head2 index

The root page (/)

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    if ( $c->{user} ) {
        $c->response->redirect('/phones/contact_list');
    }
    else {
        $c->response->redirect('/user/login');
    }
    # Hello World
    # $c->response->body( $c->welcome_message );
}

sub begin :Private {
    my ( $self, $c ) = @_;

     

	my $session = $c->session;

	if ( $session && $session->{user_id} ) {
        my $user_id = $session->{user_id};
        # Добавляем данные в STASH
        $c->stash->{user_id} = $user_id;
        
        if ( $user_id ) {
            $c->{user} = System::User->new( $user_id );
        }
    }
    # } else {
    #     unless ( $c->request->path eq '/user/login') {
    #         print Dumper ($c->request->path );
    #          return $c->res->redirect('/user/login'); }
    #     }
    
}



sub default :Path {
    my ( $self, $c ) = @_;
    $c->response->body( 'Page not found' );
    $c->response->status(404);
}



sub end : ActionClass('RenderView') {}



__PACKAGE__->meta->make_immutable;

1;
