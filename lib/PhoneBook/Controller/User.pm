package PhoneBook::Controller::User;
use Moose;
use Data::Printer;
use Data::Dumper;
use namespace::autoclean;
use System::DBI;
use System::User;


BEGIN { extends 'Catalyst::Controller'; }

sub login :Local {
    my ( $self, $c ) = @_;
   	
   	$c->stash->{response} = "Please log in.";
    print Dumper ($c->stash->{user_id});

    if ( $c->req->method eq 'POST' ) {
    	my $login    = $c->req->params->{login};
    	my $password = $c->req->params->{password};
		my $result = $c->model('Auth')->login( $login, $password);
	
		if($result->[0]{id}) {
			$c->session->{user_id} = $result->[0]{id};
			$c->response->redirect('/phones/contact_list');
			return 1;

		}
		else {
			$c->stash->{response} = "Failed to log in!";
			return 0;
		};

	};
}

sub reg :Local {
	my ( $self, $c ) = @_;

	$c->stash->{response} = "Enter new login and pass";
	
	if ( $c->req->method eq 'POST' ) {
	my $login    = $c->req->params->{login};
    my $password = $c->req->params->{password};
		if ( System::User::check_user( $login ) ) {
			$c->stash->{response} = "Username already exists!";
		} else {
			$c->stash->{response} = "Success!";
			System::User::add_user( $login, $password );
			$c->response->redirect('/user/login');
		};
	};

     

}

sub logout :Local{
	my ( $self, $c ) = @_;

	$c->response->body('You are being redirected.');
	$c->response->redirect('/user/login');
	$c->delete_session;
}


1;



