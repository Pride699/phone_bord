package PhoneBook::Controller::Phones;
use Moose;
use namespace::autoclean;
use Data::Dumper;
use Variable::Eject;
use utf8;
use System::Numbers;


BEGIN { extends 'Catalyst::Controller'; }



sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->response->body('What are you doing here?');
}

sub contact_list :Local {
	my ($self, $c) = @_;
	
	if (!$c->{user}) {
	
		$c->response->redirect('/user/login')
	} 
	else {
		if ( $c->req->method eq 'POST' ) {
			my $contact_for_del = $c->req->params->{contact_for_del};
			print Dumper $contact_for_del;
			$c->{user}->del_contact( $contact_for_del );
	    }

    	$c->stash->{contacts} = $c->{user}->get_contacts();
	
		if ( $c->req->method eq 'POST' ) {
			my $contact_for_del = $c->req->params->{contact_for_del};
			print Dumper $contact_for_del;
			$c->{user}->del_contact( $contact_for_del );
	    }
    } 

}

sub addnew :Local {
	my ($self, $c) = @_;
}

sub addnew_ajax :Local {
	my ( $self, $c ) =@_;
	
	if (!$c->{user}) {
	
		$c->response->redirect('/user/login')
	} 
	else {
		my $first_name = $c->req->params->{first_name};
		my $last_name  = $c->req->params->{last_name};
		my $city       = $c->req->params->{city};

		$c->stash->{error} = "no info added" unless ($first_name);
	    $c->stash->{error} = "no info added" unless ($last_name);
	    $c->stash->{error} = "no info added" unless ($city);
		$c->stash->{success} = 1;

		$c->{user}->add_contact( $first_name, $last_name, $city );
	 	$c->forward('View::JSON');
 	}
}

sub viewtypes :Local {
	my ( $self, $c ) = @_;
	
	if (!$c->{user}) {
	
		$c->response->redirect('/user/login')
	} 
	else {
		$c->stash->{types} = System::Numbers::get_types();
		
		my $type = $c->req->params->{type};
		
		$c->stash->{header} = System::Numbers::get_numbertype( $type );
		print Dumper ($c->stash->{header});

		$c->stash->{info} = System::Numbers::get_numbertypeinfo( $c->{user}, $type );
	}
}

sub view_numbers :Local{
	my ( $self, $c ) = @_;

	if (!$c->{user}) {
	
		$c->response->redirect('/user/login')
	} 
	else {
		my $contact_id =  $c->req->params->{contact_id};

	    $c->stash->{contactname} = $c->{user}->get_contactname( $contact_id );
	    $c->stash->{contact_id}  = $contact_id;
	  	$c->stash->{numbers}     = $c->{user}->get_numbers( $contact_id );
	 	$c->stash->{numberinfo}  = $c->{user}->get_numberinfo( $contact_id );


	}

}





__PACKAGE__->meta->make_immutable;

1;
