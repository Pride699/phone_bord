package PhoneBook::Controller::Edituser;
use Moose;
use namespace::autoclean;
use Data::Dumper;
use System::DBI;
use System::Numbers;

BEGIN { extends 'Catalyst::Controller'; }


our $hold_contact;

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;
	
	$c->response->body("I've got a baaad feeling about this!");
}




sub info :Local {
	my ( $self, $c ) = @_;

	
	if (!$c->{user}) {
	
		$c->response->redirect('/user/login')
	} 
	else {
		
		if ( $c->req->method eq 'POST' ) {
		$hold_contact = $c->req->params->{contact_id};
		};
		
		my $contact_id = $hold_contact;
		
	    if ( $c->req->method eq 'POST' and $c->req->params->{number_id}) {
	        System::Numbers::drop_mainflag( $contact_id );
	    
	        my $number_id = $c->req->params->{number_id};
	        System::Numbers::set_mainflag( $number_id );
	        $c->stash->{error} = "numbre dont exist" unless ( $number_id );
	         
	    }

		if ( $c->req->method eq 'POST' ) {
	       	my $del_number = $c->req->params->{number_for_del};
	        System::Numbers::delete_number( $c->{user}, $del_number );
	        $c->stash->{error} = "numbre dont exist" unless ( $del_number );
	    }

	    $c->stash->{contactname} = $c->{user}->get_contactname( $contact_id );
	    $c->stash->{contact_id}  = $contact_id;
	  	$c->stash->{numbers}     = $c->{user}->get_numbers( $contact_id );
	 	$c->stash->{numberinfo}  = $c->{user}->get_numberinfo( $contact_id );
 	}
}


sub add_number :Local {
	my ( $self, $c ) = @_;
	
	if (!$c->{user}) {
	
		$c->response->redirect('/user/login')
	} 
	else {
		my $contact_id = $hold_contact;
		
		$c->stash->{types} = System::Numbers::get_types();

		my $number  = $c->req->params->{number};
		my $type_id = $c->req->params->{type};
		
		System::Numbers::add_number( $c->{user}, $number, $type_id, $contact_id );
		$c->stash->{error} = "no info added" unless ($number);
	}
}

sub newtype :Local{
	my ( $self, $c ) = @_;
	
	if (!$c->{user}) {
	
		$c->response->redirect('/user/login')
	} 
	else {
			my $contact_id = $hold_contact;
			if ( $c->req->method eq 'POST' ) {
				my $newtype = $c->req->params->{new_type};
				
				$c->stash->{error} = "no info added" unless ($newtype);
			    System::Numbers::add_numbertype( $c->{user}, $newtype );
				$c->stash->{contact_id} = $contact_id;
			}
	}
}	

__PACKAGE__->meta->make_immutable;

1;
