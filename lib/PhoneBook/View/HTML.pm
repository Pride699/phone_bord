package PhoneBook::View::HTML;
use Moose;
use namespace::autoclean;


extends 'Catalyst::View::TT';

__PACKAGE__->config(
    TEMPLATE_EXTENSION => '.tt',
    ENCODING => 'utf8',
    render_die => 1,
    

);

=head1 NAME

PhoneBook::View::HTML - TT View for PhoneBook

=head1 DESCRIPTION

TT View for PhoneBook.

=head1 SEE ALSO

L<PhoneBook>

=head1 AUTHOR

pride,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
