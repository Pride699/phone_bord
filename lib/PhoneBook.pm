package PhoneBook;
use Moose;
use namespace::autoclean;
use utf8;


our @catalyst_modules = ();

BEGIN {
    push @catalyst_modules, qw(
      Session
      Session::Store::FastMmap
      Session::State::Cookie
      -Debug
      ConfigLoader
      Static::Simple
      Unicode::Encoding
      Redirect

    );
}

use Catalyst @catalyst_modules;


use Catalyst::Runtime 5.80;
use System::Cfg;

# Set flags and add plugins for the application.
#
# Note that ORDERING IS IMPORTANT here as plugins are initialized in order,
# therefore you almost certainly want to keep ConfigLoader at the head of the
# list if you're using it.
#
#         -Debug: activates the debug mode for very useful log messages
#   ConfigLoader: will load the configuration from a Config::General file in the
#                 application's home directory
# Static::Simple: will serve static files from the application's root
#                 directory

extends 'Catalyst';

our $VERSION = '0.01';

# __PACKAGE__->config({

# 'View::JSON' => {
# allow_callback => 1, # defaults to 0
# callback_param => 'cb', # defaults to 'callback'
# expose_stash => [ qw(foo bar) ], # defaults to everything
# },
# });



System::Cfg::load_config;



# Configure the application.
#
# Note that settings in phonebook.conf (or other external
# configuration file that you set up manually) take precedence
# over this when using ConfigLoader. Thus configuration
# details given here can function as a default configuration,
# with an external configuration file acting as an override for
# local deployment.

__PACKAGE__->config(
        name => 'PhoneBook',
        # Disable deprecated behavior needed by old applications
        disable_component_resolution_regex_fallback => 1,
);
__PACKAGE__->config(
    # Configure the view
    
    default_view => 'HTML', 
    'View::HTML' => {
        ENCODING => 'utf-8',
        #Set the location for TT files
        WRAPPER => 'wrapper/header.tt',


        INCLUDE_PATH => [
            __PACKAGE__->path_to( 'root', 'src' ),
        ],
    },
    'View::JSON' => {
          allow_callback  => 1,    # defaults to 0
          callback_param  => 'cb', # defaults to 'callback'
      },
);

# Start the application
__PACKAGE__->setup();

=encoding utf8

=head1 NAME

PhoneBook - Catalyst based application

=head1 SYNOPSIS

    script/phonebook_server.pl

=head1 DESCRIPTION

[enter your description here]

=head1 SEE ALSO

L<PhoneBook::Controller::Root>, L<Catalyst>

=head1 AUTHOR

pride,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
