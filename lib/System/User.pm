package System::User;
use strict;
use System::DBI;
use Data::Dumper;

sub new {
	my ( $class, $user_id ) = @_;
	my $self = {};

	bless $self, $class;
	$self->{user_id} = $user_id;

	return $self;
}

#############################################################
###################AUTHENTICATION############################


sub add_user {
    my ( $login, $password ) = @_;

    if ( check_user($login) ) {
    	return undef;
    } else {

	my $sth = dbh->prepare("INSERT INTO users (login,password) 
                                  VALUES (?,?)");

    $sth->execute($login, $password);

    my $user_id = $sth->{mysql_insertid};

    return $user_id;
	};
}

sub delete_user {
	my ( $user_id ) = @_;

	my $result = dbh->do("DELETE FROM users WHERE id = ?", undef, $user_id);

	return $result;
}

sub check_user {
	my ( $login ) = @_;

	my $result = dbh->selectall_arrayref("SELECT * FROM users where login = ?", undef, $login);

	if ($result->[0]) {
		return 1;
	} else {
		return 0;
	};

}



#######################################################################
#############################CONTACT METHODS###########################

sub get_contacts {
	my ( $self ) = @_;
	
	my $contactlist = dbh->selectall_arrayref("SELECT * FROM contacts 
		WHERE user_id = ?", {Slice => {}}, $self->{user_id});

	return $contactlist;
}


sub add_contact {
	my ( $self, $first_name, $last_name, $city ) = @_;
    
    my $contact = dbh->do("INSERT INTO contacts (user_id,first_name,last_name,city) 
                                  VALUES (?,?,?,?)", undef, $self->{user_id}, $first_name, $last_name, $city );
    
    return 1;
}

sub get_contactname {
    my ( $self, $contact_id ) =@_;
    
    my $contactname = dbh->selectall_arrayref("SELECT * FROM contacts 
        WHERE contact_id = ?", {Slice => {}}, $contact_id);
    
    return $contactname;
}

sub _check_contact {
    my (  $self, $contact_id  ) =@_;
    
    my $check_contact = dbh->selectall_arrayref("SELECT * FROM contacts 
        WHERE user_id = ? and contact_id = ?", {Slice => {}},  $self->{user_id}, $contact_id);
    print Dumper $check_contact;
    if ($check_contact->[0]) {
		return 1;
	} else {
		return 0;
	};
}

sub del_contact {
	my ( $self, $contact_id ) = @_;

	if ( _check_contact( $self, $contact_id ) ) {
		my $success = dbh->do("DELETE FROM contacts WHERE contact_id = ?", undef, $contact_id);
		return $success;
	} else {
		return undef;
	};
}

#######################################################################
###########################NUMBER METHODS##############################


sub get_numbers {
    my ( $self, $contact_id ) = @_;
    
    if ( _check_contact( $self, $contact_id ) ) {
	    my $numbers = dbh->selectall_arrayref("
	        SELECT * 
	        FROM number_data 
	        WHERE contact_id = ?",
	        {Slice => {}}, $contact_id
	    );
    	return $numbers;
	} else {
		return undef;
	}
}

sub get_numberinfo {
    my ( $self, $contact_id ) = @_;
   
    if ( _check_contact( $self, $contact_id ) ) {
	    my $numberinfo = dbh->selectall_arrayref("SELECT * FROM number_type as nt
	    JOIN number_data as nd ON ( nt.type_id = nd.type_id )
	    WHERE nd.contact_id = ?",  {Slice => {}}, $contact_id);
	    
	    return $numberinfo; 
    } else {
    	return undef;
    } 
}


1;
