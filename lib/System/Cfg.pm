package System::Cfg;
use YAML qw(LoadFile);
use Data::Dumper;
use Exporter qw( import );

# my $cfg = LoadFile("config/main.yaml");
our $cfg;
our @EXPORT = qw/cfg/;




sub load_config {
   	if ( $cfg = LoadFile("config/main.yaml") ) {
   		print ("************Success! \n");
   	} else {
   		print ("************Failed to load file! \n");
   	}
}
	# body...

sub cfg {
  my $key = shift @_;
  return $cfg->{$key} if ( $cfg->{$key} );

  load_config();

  return $cfg->{$key};

}

1;
