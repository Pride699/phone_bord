package System::Numbers;
use strict;
use System::DBI;
use Data::Dumper;

#######################################################################
###########################NUMBER METHODS##############################

sub get_types {
    # my ( $self ) = @_;

    my $types = dbh->selectall_arrayref("SELECT * FROM number_type", {Slice => {}});
    
    return $types;
}




sub add_numbertype {
    my ( $self, $newtype ) = @_;
    
    my $type = get_number($self, $newtype);
   
    if (!$type) {
        my $success = dbh->do("INSERT INTO number_type (type_name) VALUES (?) ", undef, $newtype);
        return $success;
    } 
    else {
        return 0;
    };
}

sub set_mainflag {
    my ( $number_id ) = @_;
    
    my $success = dbh->do("UPDATE number_data
        SET main_flag = 1 WHERE  number_id = ? ", undef, $number_id);

    return $success;
}


sub drop_mainflag {
    my ( $contact_id ) = @_;
    
    my $success = dbh->do("UPDATE number_data
         SET main_flag = 0 WHERE contact_id = ?", undef, $contact_id);

    return $success;
}

sub add_number {
    my ( $self, $number, $type_id, $contact_id ) = @_;
   
    my $success = dbh->do("INSERT INTO number_data (number,type_id,contact_id) 
                                  VALUES (?,?,?)", undef, $number, $type_id, $contact_id );

    return $success;

}


sub get_number {
    my ($self, $name) = @_;
    
    my $type = dbh->selectall_arrayref("SELECT * FROM number_type WHERE type_name = ?", {Slice => {}}, $name);
    
    return $type->[0] ? $type->[0] : 0;

}

sub _check_number {
    my (  $self, $number_id  ) =@_;
    
    my $check_number = dbh->selectall_arrayref("SELECT * FROM number_data as nd 
    	JOIN contacts as c on (nd.contact_id = c.contact_id)
    	WHERE c.user_id = ? and nd.number_id = ?", {Slice => {}},  $self->{user_id}, $number_id);
    if ($check_number->[0]) {
		return 1;
	} else {
		return 0;
	};
}


sub delete_number {
    my ( $self, $number_id ) = @_;

    if (_check_number( $self, $number_id )) {
    	my $success = dbh->do("DELETE FROM number_data WHERE number_id = ?", undef, $number_id );
    	return $success;
    } else {
    	return undef;
	};
}

sub get_numbertypeinfo {
    my ( $self, $type_id ) = @_;
   
    my $info = dbh->selectall_arrayref("SELECT * FROM number_type as nt
    JOIN number_data as nd ON ( nt.type_id = nd.type_id )
    JOIN contacts as c ON ( c.contact_id = nd.contact_id )
    WHERE nt.type_id = ? and c.user_id = ?",  {Slice => {}}, $type_id, $self->{user_id} );
    
    return $info; 
}

sub get_numbertype {
 	my ( $type_id ) = @_;
    
    my $typename = dbh->selectall_arrayref("SELECT * FROM number_type 
        WHERE type_id = ?", {Slice => {}} , $type_id);
    
    return $typename;
}



1;
