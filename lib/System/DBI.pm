package System::DBI;
use Moose;
use DBI;
use Data::Printer;
use Exporter qw( import );


use System::Cfg;


our $connect;
our @EXPORT = qw/dbh/;


sub connect_dbh {  
    my $cfg = cfg("db");
	$connect = DBI->connect("dbi:mysql:$cfg->{name}", "$cfg->{user}", "$cfg->{pass}");
	print("************Success \n");

	return $connect;
}

sub dbh {
	return $connect if ($connect);
	connect_dbh();
}



1;
