use strict;
use warnings;
use Test::More;
use Data::Dumper;
use Data::Printer;
use lib::abs qw(../../lib);
use System::User;
use System::Numbers;
use PhoneBook::Model::Auth;

require_ok( 'System::User' );

my $test_user_login = "test_user";
my $test_user_pass  = "password";

my $new_user_id = System::User::add_user( $test_user_login, $test_user_pass );
ok($new_user_id, "create test user");
ok(!System::User::add_user( $test_user_login, $test_user_pass ), "create existing login");

print Dumper $new_user_id;

my $test_self = System::User->new( $new_user_id );
print Dumper $test_self->{user_id}; 


not ok(System::User::add_contact( $test_self, undef, undef, 'Moscow'), "create empty contact");
ok(!System::Numbers::add_number( undef, undef, 1, 1 ), "create empty number");


my $contact_id = 1;                        ##This contact belongs to User 1;
ok(!System::User::del_contact( $test_self, $contact_id ), "delete foreign contact");
ok(!System::User::get_numbers( $test_self, $contact_id ), "look numbers of foreign contact");
ok(!System::User::get_numberinfo( $test_self, $contact_id ), "get foreign number information");

my $number_id = 2;                         ##This number belongs to phonebook of User 1;

ok(System::User::add_contact( $test_self, 'test1name', 'test2name', 'defaultcity'), "create test contact");
print Dumper (System::User::add_contact( $test_self, 'test1name', 'test2name', 'defaultcity'));

ok(System::User::get_contacts( $test_self), "see test contacts");
print Dumper (System::User::get_contacts( $test_self));

my $fake_login = "I dont exist";
my $fake_pass = "I open nothing";
not ok(PhoneBook::Model::Auth::login( $test_self, $fake_login, $fake_pass), "logging with fake info");


System::User::delete_user( $new_user_id );
done_testing();

1;