use strict;
use warnings;
use Test::More;
use Data::Dumper;
use Data::Printer;
use lib::abs qw(../../lib);
use System::User;
use System::Numbers;
use PhoneBook::Model::Auth;

require_ok( 'System::User' );

my $test_user_login = "test_user";
my $test_user_pass  = "password";

my $new_user_id = System::User::add_user( $test_user_login, $test_user_pass );
ok($new_user_id, "create test user");


print Dumper $new_user_id;

my $test_self = System::User->new( $new_user_id );
print Dumper $test_self->{user_id}; 



ok(!System::Numbers::add_numbertype( $test_self, undef), "create empty number type");



my $number_id = 2;                         ##This number belongs to phonebook of User 1;
ok(!System::Numbers::delete_number( $test_self, $number_id), "delete foreign number");




System::User::delete_user( $new_user_id );
done_testing();

1;